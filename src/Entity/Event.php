<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity(repositoryClass="App\Repository\EventRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Event
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank(message="Can`t empty field name ")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Can`t empty field description ")
     */

    private $description;

    /**
     * @ORM\Column(type="decimal", precision=9, scale=2)
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $city;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank(message="Can`t empty field date ")
     */
    private $date;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message="Can`t empty field numTickets ")
     */
    private $numTickets;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $numTicketsDisp;

    /**
     * @return mixed
     */
    public function getNumTicketsDisp()
    {
        return $this->numTicketsDisp;
    }

    /**
     * @ORM\PrePersist
     */
    public function firstNumTicketsDisp()
    {
        $this->numTicketsDisp = $this->numTickets;
    }

    public function setNumTicketsDisp($numTicketsDisp)
    {
        $this->numTicketsDisp = $numTicketsDisp;
    }

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank(message="Can`t empty field date inicio ")
     */
    private $dateIni;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank(message="Can`t empty field date finish ")
     */
    private $dateFinish;

    /**
     * @ORM\Column(type="string")
     */
    private $imagen;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="events")
     */
    private $creator;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category")
     * @Assert\NotBlank(message="Please select category")
     */
    private $categoria;

    /**
     * @return mixed
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * @param mixed $categoria
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getNumTickets()
    {
        return $this->numTickets;
    }

    /**
     * @param mixed $numTickets
     */
    public function setNumTickets($numTickets)
    {
        $this->numTickets = $numTickets;
    }

    /**
     * @return mixed
     */
    public function getDateIni()
    {
        return $this->dateIni;
    }

    /**
     * @param mixed $dateIni
     */
    public function setDateIni($dateIni)
    {
        $this->dateIni = $dateIni;
    }

    /**
     * @return mixed
     */
    public function getDateFinish()
    {
        return $this->dateFinish;
    }

    /**
     * @param mixed $dateFinish
     */
    public function setDateFinish($dateFinish)
    {
        $this->dateFinish = $dateFinish;
    }

    /**
     * @return mixed
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * @param mixed $imagen
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;
    }

    /**
     * @return mixed
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @param mixed $creator
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;
    }

    // add your own fields
}
