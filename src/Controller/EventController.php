<?php

namespace App\Controller;

use App\BLL\CategoryBLL;
use App\Form\EventType;
use App\Service\FileUploader;
use Doctrine\ORM\Mapping as ORM;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use App\BLL\EventBLL;
use App\Entity\Event;

/**
 *
 * @ORM\Entity
 * @ORM\Table(name="event_controller")
 */
class EventController extends Controller
{
    /**
     * @Route("event/detail/{id}", name="event-detail")
     * @Template("events/eventDetail.html.twig")
     */
    public function showEvent($id,EventBLL $eventBLL)
    {
        $event = $eventBLL->getEvent($id);
        return [
            "event"=> $event
        ];
    }

    /**
     * @Route("event/category/{category}", name="event-list-category")
     * @Template("events/eventList.html.twig")
     */
    public function showListCategory(EventBLL $eventBLL, $category, CategoryBLL $categoryBLL)
    {
        $categories = $categoryBLL->getCategories();
        $events = $eventBLL->getEventsCategory($category);
        return [
            "categories"=>$categories,
            "events"=> $events
        ];
    }
    /**
     * @Route("event/search", name="event-list-search")
     * @Method("POST")
     * @Template("events/eventList.html.twig")
     */
    public function buscar(Request $request, EventBLL $eventBLL, CategoryBLL $categoryBLL)
    {
        $search = $request->request->get('search');
        $events = $eventBLL->getEventsSearch($search);
        $categories = $categoryBLL->getCategories();
        return [
            "events" => $events,
            "categories" =>$categories
        ];
    }
    /**
     * @Route("/", name="event-list")
     * @Template("events/eventList.html.twig")
     */
    public function showList(EventBLL $eventBLL, CategoryBLL $categoryBLL)
    {
        $categories = $categoryBLL->getCategories();
        $events = $eventBLL->getEvents();
        return [
            "categories"=>$categories,
            "events"=> $events
        ];
    }
    /**
     * @Route("event/edit/{id}", name="event-edit")
     * @Template("events/newEvent.html.twig")
     * @IsGranted("ROLE_GESTOR")
     */
    public function edit(Request $request ,EventBLL $eventBLL, Event $event)
    {
        $event->setImagen(
            new File(
                $this->getParameter('events') . '/'.$event->getImagen()
            )
        );
        return $this->formEvent($request, $eventBLL, $event);
    }

    /**
     * @Route("event/eliminar/{id}", name="event-delete")
     * @IsGranted("ROLE_GESTOR")
     */
    public function delete(Request $request ,EventBLL $eventBLL,Event $event)
    {
        if($event->getNumTickets() != $event->getNumTicketsDisp())
            throw $this->createAccessDeniedException("There are tickets sold");
        $eventBLL->deleteEvent($event);

        return $this->redirectToRoute('event-list');
    }
    /**
     * @Route("event/new", name="new-event")
     * @IsGranted("ROLE_GESTOR")
     */
    public function nuevo(Request $request, EventBLL $eventBLL)
    {
        $event = new Event();
        $event->setCreator($this->getUser());
        return $this->formEvent($request, $eventBLL, $event);
    }

    private function formEvent(Request $request, EventBLL $eventBLL, Event $event)
    {
        $form = $this->createForm(EventType::class, $event);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $eventBLL->createEvent($event);
            return $this->redirectToRoute('event-list');
        }

        return $this->render('events/newEvent.html.twig',[
            'form' => $form->createView()
        ]);

    }
}
