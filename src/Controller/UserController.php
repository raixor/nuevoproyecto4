<?php

namespace App\Controller;

use App\BLL\UserBLL;
use App\Entity\User;
use App\Form\UserType;
use Doctrine\ORM\Mapping as ORM;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/user", name="user")
 * @ORM\Entity
 * @ORM\Table(name="user_controller")
 * @UniqueEntity("email")
 */
class UserController extends Controller
{
    /**
     * @Route("/list", name="list-user")
     * @Template("users/userList.html.twig")
     * @IsGranted("ROLE_ADMIN")
     */
    public function listUsers(UserBLL $userBLL)
    {
        $roles =$this->getParameter('security.role_hierarchy.roles');
        $users = $userBLL->getUsers();
        dump($roles);
        return [ "users" => $users, "roles" => $roles];
    }
    /**
     * @Route("/list/search", name="list-user-search")
     * @Template("users/userList.html.twig")
     * @IsGranted("ROLE_ADMIN")
     */
    public function listUsersSearch(Request $request ,UserBLL $userBLL)
    {
        $search = $request->request->get('search');
        $roles =$this->getParameter('security.role_hierarchy.roles');
        $users = $userBLL->getUsersSearch($search);
        return [ "users" => $users, "roles" => $roles];
    }

    /**
     * @Route("/list/{role}", name="list-user-roles", requirements={"role"="ROLE_GESTOR|ROLE_COMPRADOR|ROLE_ADMIN"})
     * @Template("users/userList.html.twig")
     * @IsGranted("ROLE_ADMIN")
     */
    public function listUsersRoles(UserBLL $userBLL,$role)
    {
        $roles =$this->getParameter('security.role_hierarchy.roles');
        $users = $userBLL->getUsers($role);
        return [ "users" => $users, "roles" => $roles];
    }



    /**
     * @Route("/detail/{id}", name="detail-user")
     * @Template("users/userDetail.html.twig")
     *
     */
    public function detailUser(User $user)
    {
        return ["user"=>$user];
    }

    /**
     * @Route("/register/{role}", name="register", requirements={"role"="ROLE_GESTOR|ROLE_COMPRADOR"}))
     * @Template("users/newUser.html.twig")
     */
    public function newUser($role,Request $request, UserBLL $userBLL, UserPasswordEncoderInterface $encoder)
    {
        $user = new User();
        $user->setRol($role);
        return $this->formUser($request, $userBLL, $user, $encoder);
    }

    /**
     * @Route("/edit", name="edit-user")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     * @Template("users/newUser.html.twig")
     */
    public function editProfile(Request $request, UserBLL $userBLL, UserPasswordEncoderInterface $encoder)
    {
        $user = $userBLL->getProfile($this->getUser()->getId());
        $user->setAvatar(
            new File(
                $this->getParameter('users').'/'.$user->getAvatar()
            )
        );
        return $this->formUser($request, $userBLL, $user, $encoder);
    }

    /**
     * @Route("/edit/rol/{id}", name="edit-user-rol", requirements={"id": "\d+"})
     * @IsGranted("ROLE_ADMIN")
     * @Method("POST")
     */
    public function editRole(Request $request, UserBLL $userBLL, User $user)
    {
        $rol= $request->request->get('rol');
        if($rol != "ROLE_ADMIN" && $user->getRol() != "ROLE_ADMIN")
        {
            $user->setRol($rol);
            $userBLL->createUser($user);
            return $this->redirectToRoute('userlist-user');
        }
        return $this->redirectToRoute('userlist-user');
    }
    /**
     * @Route("/edit/password", name="edit-user-password")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     *
     */
    public function editPassword(Request $request, UserBLL $userBLL)
    {
        if($request->request->get('password'))
        {
            $password= $request->request->get('password');
            $userBLL->cambiaPassword($password);
            return $this->redirectToRoute('event-list');
        }
        return $this->render('users/editPassword.html.twig',[]);
    }

    /**
     * @Route("/edit/email", name="edit-user-email")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function editEmail(Request $request, UserBLL $userBLL)
    {
        if($request->request->get('email'))
        {
            $email= $request->request->get('email');
            $userBLL->cambiaEmail($email);
            return $this->redirectToRoute('event-list');
        }
        return $this->render('users/editEmail.html.twig',[]);
    }



    private function formUser(Request $request, UserBLL $userBLL, User $user, UserPasswordEncoderInterface $encoder)
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $file = $user->getAvatar();
            $fileName = md5(uniqid()).'.'.$file->guessExtension();
            $file->move(
                $this->getParameter('users'),
                $fileName
            );
            $password = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($password);
            $user->setAvatar($fileName);
            $userBLL->createUser($user);
            return $this->redirectToRoute('login');
        }

        return $this->render('users/newUser.html.twig',[
            'form' => $form->createView()
        ]);
    }


}
