<?php

namespace App\Controller;

use App\BLL\EventBLL;
use App\BLL\InvoiceBLL;
use App\Entity\Invoice;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/invoice")
 */
class InvoiceController extends Controller
{
    /**
     * @Route("/create/{id}", name="invoice-create", requirements={"id" : "\d+"})
     * @Method("POST")
     * @IsGranted("ROLE_COMPRADOR")
     */
    public function create(Request $request,InvoiceBLL $invoiceBLL, EventBLL $eventBLL, $id)
    {
        // replace this line with your own code!
        $tickets= $request->request->get('tickets');
        $event = $eventBLL->getEvent($id);
        if($event)
        {
            if($event->getNumTicketsDisp() < $tickets)
            {
                throw $this->createNotFoundException('Too much tickets.');
            }
            $saveInvoice = $invoiceBLL->save( $tickets, $event);
            return $this->render('invoice/tikets-list.html.twig', ['invoice' => $saveInvoice, 'buynow' => $tickets]);
        }else
        {
            throw $this->createNotFoundException('The event does not exist');
        }
    }

    /**
     * @Route("/", name="invoice-list")
     * @Method("GET")
     * @IsGranted("ROLE_COMPRADOR")
     * @Template("invoice/tikets.html.twig")
     */
    public function tickets(Request $request, InvoiceBLL $invoiceBLL){
        $invoices = $invoiceBLL->getInvoicesYour($this->getUser());
        return [
          'invoices' => $invoices
        ];
    }
}
