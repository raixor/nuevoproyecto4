<?php
/**
 * Created by PhpStorm.
 * User: georgealmeida
 * Date: 19/02/18
 * Time: 12:21
 */

namespace App\Controller\REST;


use App\BLL\InvoiceBLL;
use App\Entity\Event;
use App\Entity\Invoice;
use App\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class InvoicesRestController extends BaseApiController
{
    /**
     * @Route("invoice/create/{id}.{_format}", name="invoice-create-api",
     *  defaults={"_format": "json"},
     *  requirements={"_format": "json", "id" : "\d+"}
     * )
     * @Method("POST")
     * @IsGranted("ROLE_COMPRADOR")
     */
    public function post(Event $event , InvoiceBLL $invoiceBLL, Request $request)
    {
        $data = $this->getContent($request);

        $invoice = $invoiceBLL->nuevo($data['tickets'], $event);
        return $this->getResponse($invoice,Response::HTTP_CREATED);
    }

    /**
     * @Route("/invoices", name="invoices-my",
     *  defaults={"_format": "json"},
     *  requirements={"_format": "json", "id" : "\d+"}
     * )
     * @Method("GET")
     * @IsGranted("ROLE_COMPRADOR")
     */
    public function getMyInvoices(InvoiceBLL $invoiceBLL)
    {
        $invoices = $invoiceBLL->getInvoices($this->getUser());
        return $this->getResponse($invoices,Response::HTTP_CREATED);
    }

    /**
     * @Route("/invoices/{id}", name="invoices-event",
     *  defaults={"_format": "json"},
     *  requirements={"_format": "json", "id" : "\d+"}
     * )
     * @Method("GET")
     * @IsGranted("ROLE_GESTOR")
     */
    public function getInvoicesEvent(InvoiceBLL $invoiceBLL, Event $event)
    {
        $invoices = $invoiceBLL->getInvoicesEvent($event);
        return $this->getResponse($invoices,Response::HTTP_CREATED);
    }

}