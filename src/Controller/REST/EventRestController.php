<?php
/**
 * Created by PhpStorm.
 * User: georgealmeida
 * Date: 18/02/18
 * Time: 12:51
 */

namespace App\Controller\REST;


use App\BLL\EventBLL;
use App\Entity\Event;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EventRestController extends BaseApiController
{
    /**
     * @Route("/events.{_format}", name="post_events",
     *  defaults={"_format": "json"},
     *  requirements={"_format": "json"}
     * )
     * @IsGranted("ROLE_GESTOR")
     * @Method("POST")
     */
    public function post(Request $request, EventBLL $eventBLL)
    {
        $data = $this->getContent($request);

        $event = $eventBLL->nuevo(
            $data['name'],
            $data['numTickets'],
            $data['imagen'],
            $data['creator'],
            $data['categoria'],
            $data['city'],
            $data['date'],
            $data['description'],
            $data['dateIni'],
            $data['dateFinish'],
            $data['price']
        );
        return $this->getResponse($event,Response::HTTP_CREATED);
    }

    //TODO no funciona la ruta order mas dos parametros

    /**
     * @Route("/events.{_format}", name="get_events",
     * defaults={"_format": "json"},
     * requirements={"_format": "json"}
     * )
     * @Route("/events/{order1}/{order2}", name="get_events_ordenados",
     * requirements={"order1": "price|dateIni", "order1": "dateFinish|date"})
     * @IsGranted("ROLE_GESTOR")
     * @Method("GET")
     */

    public function getAll(Request $request, EventBLL $eventBLL, $order1='price', $order2='date')
    {
        $city = $request->query->get('city');
        $price = $request->query->get('price');

        $events = $eventBLL->getEventsFiltrados($city, $price, $order1, $order2);
        return $this->getResponse($events);
    }

    /**
     * @Route("/events/{id}.{_format}", name="delete_events",
     * defaults={"_format": "json"},
     * requirements={"_format": "json", "id" : "\d+"}
     * )
     * @IsGranted("ROLE_GESTOR")
     * @Method("DELETE")
     */
    public function delete(Event $event, EventBLL $eventBLL)
    {
        if($this->getUser() == $event->getCreator())
        {
            $eventBLL->delete($event);
            return $this->getResponse(null,Response::HTTP_NO_CONTENT);
        }else{
            return $this->getResponse(null,Response::HTTP_NOT_ACCEPTABLE);
        }
    }

    /**
     * @Route("/events/{id}.{_format}", name="update_events",
     * defaults={"_format": "json"},
     * requirements={"_format": "json", "id" : "\d+"}
     * )
     * @IsGranted("ROLE_GESTOR")
     * @Method("PUT")
     */
    public function update(Request $request, Event $event, EventBLL $eventBLL)
    {
        if($this->getUser() == $event->getCreator())
        {
            $data = $this->getContent($request);
            $event = $eventBLL->update($event, $data);

            return $this->getResponse($event,Response::HTTP_CREATED);
        }else{
            return $this->getResponse(null,Response::HTTP_NOT_ACCEPTABLE);
        }
    }
}