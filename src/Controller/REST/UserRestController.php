<?php
/**
 * Created by PhpStorm.
 * User: georgealmeida
 * Date: 18/02/18
 * Time: 17:59
 */

namespace App\Controller\REST;


use App\BLL\UserBLL;
use App\Entity\Event;
use App\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class UserRestController extends BaseApiController
{
    /**
     * @Route("/profile.{_format}", name="profile",
     * requirements={
     * "_format": "json"
     * },
     * defaults={"_format": "json"})
     * @Method("GET")
     */
    public function profile(UserBLL $userBLL)
    {
        $user = $userBLL->profile();
        return $this->getResponse($user);
    }

    /**
     * @Route("/profile/password.{_format}", name="cambia_password",
     * requirements={
     * "_format": "json"
     * },
     * defaults={"_format": "json"})
     * @IsGranted("ROLE_ADMIN")
     * @Method("PATCH")
     */
    public function cambiaPassword(Request $request, UserBLL $userBLL)
    {
        $data = $this->getContent($request);
        if (is_null($data['password']) || !isset($data['password']) || empty($data['password']))
            throw new BadRequestHttpException('No se ha recibido el password');
        $user = $userBLL->cambiaPassword($data['password']);
        return $this->getResponse($user);
    }
    /**
     * @Route("auth/register.{_format}", name="register",
     *  defaults={"_format": "json"},
     *  requirements={"_format": "json"}
     * )
     * @Method("POST")
     */
    public function post(Request $request, UserBLL $userBLL)
    {
        $data = $this->getContent($request);

        $user = $userBLL->nuevo(
            $data['name'],
            $data['email'],
            $data['rol'],
            $data['ciudad'],
            $data['avatar'],
            $data['password'],
            $this->getParameter('users')
        );
        return $this->getResponse($user,Response::HTTP_CREATED);
    }

    /**
     * @Route("user/nuevo.{_format}", name="nuevo_user",
     *  defaults={"_format": "json"},
     *  requirements={"_format": "json"}
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Method("POST")
     */
    public function nuevo(Request $request, UserBLL $userBLL)
    {
        return $this->post($request, $userBLL);
        /*$data = $this->getContent($request);

        $user = $userBLL->nuevo(
            $data['name'],
            $data['email'],
            $data['rol'],
            $data['ciudad'],
            $data['avatar'],
            $data['password'],
            $this->getParameter('users')
        );
        return $this->getResponse($user,Response::HTTP_CREATED);*/
    }
    /**
     * @Route("user/{id}.{_format}", name="update_user",
     *  defaults={"_format": "json"},
     *  requirements={"_format": "json", "id" : "\d+"}
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Method("PUT")
     */
    public function update(Request $request, UserBLL $userBLL, User $user)
    {
        $data = $this->getContent($request);

        $user = $userBLL->update($user, $data, $this->getParameter('users'));
        return $this->getResponse($user,Response::HTTP_CREATED);
    }



    /**
     * @Route("/user/{id}.{_format}", name="get_usuario",
     * requirements={
     * "id": "\d+",
     * "_format": "json"
     * },
     * defaults={"_format": "json"})
     * @IsGranted("ROLE_ADMIN")
     * @Method("GET")
     */
    public function getOne(User $user, UserBLL $userBLL)
    {
        return $this->getResponse($userBLL->toArray($user));
    }

    /**
     * @Route("/users.{_format}", name="get_usuarios",
     * defaults={"_format": "json"},
     * requirements={"_format": "json"}
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Method("GET")
     */
    public function getAll(UserBLL $userBLL)
    {
        $users = $userBLL->getAll();
        return $this->getResponse($users);
    }

    /**
     * @Route("/user/{id}.{_format}", name="delete_usuario",
     * requirements={
     * "id": "\d+",
     * "_format": "json"
     * },
     * defaults={"_format": "json"}))
     * @IsGranted("ROLE_ADMIN")
     * @Method("DELETE")
     */
    public function delete(User $user, UserBLL $userBLL)
    {
        $userBLL->delete($user);
        return $this->getResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/auth/login/google")
     */
    public function googleLogin(Request $request, UserBLL $userBLL)
    {
        $data = $this->getContent($request);
        if (is_null($data['access_token']) || !isset($data['access_token']) ||
            empty($data['access_token']))
            throw new BadRequestHttpException('No se ha recibido el token de google');
        $googleJwt = json_decode(file_get_contents(
            "https://www.googleapis.com/plus/v1/people/me?access_token=" .
            $data['access_token']));
        $token = $userBLL->getTokenByEmail($googleJwt->emails[0]->value);
        return $this->getResponse(['token' => $token]);
    }
}