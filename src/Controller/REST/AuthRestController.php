<?php
/**
 * Created by PhpStorm.
 * User: georgealmeida
 * Date: 18/02/18
 * Time: 19:05
 */

namespace App\Controller\REST;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class AuthRestController
{
    /**
     * @Route("/auth/login")
     */
    public function getTokenAction()
    {
        // The security layer will intercept this request
        return new Response('', Response::HTTP_UNAUTHORIZED);
    }
}