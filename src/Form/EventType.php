<?php
/**
 * Created by PhpStorm.
 * User: georgealmeida
 * Date: 7/02/18
 * Time: 13:12
 */

namespace App\Form;


use App\Entity\Category;
use App\Entity\Event;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EventType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('description', TextType::class)
            ->add('price', MoneyType::class)
            ->add('city', TextType::class)
            ->add('date', DateType::class)
            ->add('numTickets', IntegerType::class)
            ->add('dateIni', DateType::class)
            ->add('dateFinish', DateType::class)
            ->add('imagen', FileType::class, array(
                'label' => 'Image (PNG o JPG)'))
            ->add('categoria', EntityType::class, ['class' => Category::class])
            ->add('Create', SubmitType::class, [
                'label' => 'Create'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Event::class,
        ));
    }
}