<?php

namespace App\Repository;

use App\Entity\Event;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class EventRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Event::class);
    }

    public function findEvents($busqueda)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->where($qb->expr()->like('e.name',':busqueda'))
            ->setParameter('busqueda', '%'.$busqueda.'%');
        $qb->orWhere($qb->expr()->like('e.description',':busqueda'))
            ->setParameter('busqueda', '%'.$busqueda.'%');
        return $qb->getQuery()->getResult();

    }

    public function getEventsFiltrados($city, $price, $order1, $order2, $id)
    {
        $qb = $this->createQueryBuilder('event');
        $qb->where('event.creator = :id')->setParameter('id',$id);

        if(isset($city))
        {
            $qb->where($qb->expr()->like('event.city', ':city'))
                ->setParameter('city', '%' . $city . '%');
        }
        if(isset($price))
        {
            $qb->andWhere($qb->expr()->lte('event.price', ':price'))
                ->setParameter('price', $price);
        }
        $qb->orderBy('event.' . $order1, 'ASC')
            ->orderBy('event.' . $order2, 'DESC');
        return $qb->getQuery()->getResult();
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('e')
            ->where('e.something = :value')->setParameter('value', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
