<?php
/**
 * Created by PhpStorm.
 * User: georgealmeida
 * Date: 18/02/18
 * Time: 16:58
 */

namespace App\EventListener;


use App\Entity\Event;
use App\Service\FileUploader;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageUploadListener
{
    private $uploader;

    public function __construct(FileUploader $uploader)
    {
        $this->uploader = $uploader;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->uploadFile($entity);
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->uploadFile($entity);
    }

    private function uploadFile($entity)
    {
        if (!$entity instanceof Event)
            return;

        $file = $entity->getImagen();

        if ($file instanceof UploadedFile)
        {
            $fileName = $this->uploader->upload($file);

            $entity->setImagen($fileName);
        }
    }

    public function postLoad(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if (!$entity instanceof Event)
            return;

        if ($fileName = $entity->getImagen())
        {
            $entity->setImagen(
                new File(
                    $this->uploader->getTargetDir().'/'.$fileName
                )
            );
        }
    }
}