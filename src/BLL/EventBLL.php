<?php
/**
 * Created by PhpStorm.
 * User: georgealmeida
 * Date: 7/02/18
 * Time: 13:05
 */

namespace App\BLL;

use App\Entity\Category;
use App\Entity\Event;
use App\Entity\User;
use App\Service\FileUploader;

class EventBLL extends BaseBLL
{
    /**
     * @var FileUploader $uploader
     */
    private $uploader;

    public function setUploader(FileUploader $uploader)
    {
        $this->uploader = $uploader;
    }

    public function createEvent( Event $event)
    {
        $this->em->persist($event);
        $this->em->flush();
    }

    public function getEvents(){
        return $this->em->getRepository(Event::class)->findAll();
    }

    public function getEvent($id){
        return $this->em->getRepository(Event::class)->find($id);
    }

    public function getEventsCategory($category)
    {
        return $this->em->getRepository(Event::class)->findBy(['categoria' => $category]);
    }

    public function getEventsSearch($search)
    {
        return $this->em->getRepository(Event::class)->findEvents($search);
    }

    public function deleteEvent(Event $event)
    {
        $this->em->remove($event);
        $this->em->flush();
    }

    public function nuevo($name, $numTickets, $imagen, $creator, $categoria, $city, $date, $description, $dateIni, $dateFinish,$price)
    {
        $event = new Event();
        $event->setNumTickets($numTickets);
        $event->setDescription($description);
        $event->setCity($city);
        $event->setDate(new \DateTime($date));
        $event->setDateIni(new \DateTime($dateIni));
        $event->setDateFinish(new \DateTime($dateFinish));
        $event->setName($name);
        $event->setPrice($price);

        $filename = $this->uploader->saveB64Imagen($imagen);
        $event->setImagen($filename);

        $categoriaE = $this->em->getRepository(Category::class)->findOneBy(['name' => $categoria]);
        $userE = $this->em->getRepository(User::class)->findOneBy(['id' => $creator]);
        $event->setCategoria($categoriaE);
        $event->setCreator($userE);

        return $this->guardaValidando($event);
    }
    public function toArray($event)
    {
        if(is_null($event))
            return null;

        if (!($event instanceof Event))
            throw new \Exception("La entidad no es un Evento");

        return [
            'id' => $event->getId(),
            'name' => $event->getName(),
            'numTickets'=>$event->getNumTickets(),
            'imagen'=>$event->getImagen(),
            'creator'=>$event->getCreator()->getId(),
            'categoria'=>$event->getCategoria()->getName(),
            'city'=>$event->getCity(),
            'description'=>$event->getDescription(),
            'date'=>$event->getDate(),
            'dateIni'=>$event->getDateIni(),
            'dateFinish'=>$event->getDateFinish(),
            'price'=>$event->getPrice()
        ];
    }

    public function getEventsFiltrados($city, $price, $order1, $order2)
    {
        $events = $this->em->getRepository(Event::class)->getEventsFiltrados(
            $city, $price, $order1, $order2,$this->getUser()->getId()
        );
        return $this->entitiesToArray($events);
    }

    public function update(Event $event, $data)
    {
        $event->setName($data['name']);
        $event->setPrice($data['price']);
        $event->setDate(new \DateTime($data['date']));
        $event->setDescription($data['description']);
        $event->setCity($data['city']);
        $event->setDateFinish(new \DateTime($data['dateFinish']));

        $categoria = $this->em->getRepository(Category::class)->findOneBy(['name' => $data['category']]);
        $event->setCategoria($categoria);

        //TODO se puede cambiar el creador?
        /*$creator = $this->em->getRepository(User::class)->find($data['creator']);
        $event->setCreator($creator);*/

        $nameFile = $this->uploader->saveB64Imagen($data['imagen']);
        $event->setImagen($nameFile);

        return $this->guardaValidando($event);
    }
}