<?php
/**
 * Created by PhpStorm.
 * User: georgealmeida
 * Date: 12/02/18
 * Time: 12:44
 */

namespace App\BLL;


use App\Entity\Category;
use Doctrine\Common\Persistence\ObjectManager;

class CategoryBLL extends BaseBLL
{
    public function getCategories(){
        return $this->em->getRepository(Category::class)->findAll();
    }

    public function toArray($entity)
    {
        // TODO: Implement toArray() method.
    }
}