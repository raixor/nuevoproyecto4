<?php
/**
 * Created by PhpStorm.
 * User: georgealmeida
 * Date: 15/02/18
 * Time: 13:46
 */

namespace App\BLL;


use App\Entity\Event;
use App\Entity\Invoice;
use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;

class InvoiceBLL extends BaseBLL
{
    //TODO guardar entradas comprobaciones antes y que te tiene que psara
    public function save($tickets,Event $event)
    {
        $invoice = new Invoice();
        $invoice->setEvent($event);
        $invoice->setUser($this->getUser());
        if($this->em->getRepository(Invoice::class)->findOneBy(['event' => $invoice->getEvent(), 'user' => $invoice->getUser()])){
            $invoice = $this->em->getRepository(Invoice::class)->findOneBy(['event' => $invoice->getEvent(), 'user' => $invoice->getUser()]);
            $invoice->setNumTickets($invoice->getNumTickets() + $tickets);
            $this->em->persist($invoice);
            $this->em->flush();

        }else{
            $invoice->setNumTickets($tickets);
            $this->em->persist($invoice);
            $this->em->flush();
        }
        $event->setNumTicketsDisp($event->getNumTicketsDisp()-$tickets);
        $this->em->persist($event);
        $this->em->flush();

        return $invoice;
    }

    public function getInvoicesYour(User $user)
    {
        return $this->em->getRepository(Invoice::class)->findBy(['user' => $user]);
    }

    public function toArray($invoice)
    {
        // TODO: Implement toArray() method.
        if (is_null($invoice))
            return null;
        if (!($invoice instanceof Invoice))
            throw new \Exception("La entidad no es Invoice");
        return [
            'id' => $invoice->getId(),
            'numTickets' => $invoice->getNumTickets(),
            'event' => $invoice->getEvent()->getName(),
            'user' => $invoice->getUser()->getId()
        ];
    }

    public function nuevo($tickets, Event $event)
    {
        $invoice = new Invoice();
        $invoice->setEvent($event);
        $invoice->setUser($this->getUser());
        $this->save($invoice, $tickets);
        return $this->toArray($invoice);
    }

    public function getInvoices(User $user)
    {
        $invoices = $this->getInvoicesYour($user);
        return $this->entitiesToArray($invoices);
    }

    public function getInvoicesEvent(Event $event)
    {
        return $this->entitiesToArray($this->em->getRepository(Invoice::class)->findBy(['event' => $event]));
    }
}