<?php
/**
 * Created by PhpStorm.
 * User: georgealmeida
 * Date: 7/02/18
 * Time: 20:24
 */

namespace App\BLL;


use App\Entity\User;
use App\Service\FileUploader;
use Doctrine\Common\Persistence\ObjectManager;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserBLL extends BaseBLL
{
    /**
     * @var FileUploader $uploader
     */
    private $uploader;

    public function setUploader(FileUploader $uploader)
    {
        $this->uploader = $uploader;
    }

    /** @var UserPasswordEncoderInterface $encoder
     */
    private $encoder;
    public function setEncoder(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * @var JWTTokenManagerInterface
     */
    private $jwtManager;

    public function setJWTManager(JWTTokenManagerInterface $jwtManager)
    {
        $this->jwtManager = $jwtManager;
    }
    public function createUser(User $user)
    {
        $this->em->persist($user);
        $this->em->flush();
    }

    public function getUsers($role = null )
    {
        if($role)
            return $this->em->getRepository(User::class)->findBy(["rol" => $role]);
        return $this->em->getRepository(User::class)->findAll();
    }

    public function getProfile($id){
        return $this->em->getRepository(User::class)->find($id);
    }

    public function nuevo($name, $email, $rol, $ciudad, $avatar, $password, $dir)
    {
        $user = new User();
        $user->setName($name);
        $user->setEmail($email);
        $user->setCiudad($ciudad);
        $user->setPassword($this->encoder->encodePassword($user,$password));

        if($rol == "ROLE_COMPRADOR" || $rol == "ROLE_GESTOR")
            $user->setRol($rol);
        else
            $user->setRol("ROLE_COMPRADOR");

        $this->uploader->setTargetDir($dir);
        $fileName = $this->uploader->saveB64Imagen($avatar);
        $user->setAvatar($fileName);

        return $this->guardaValidando($user);
    }

    public function toArray($user)
    {
        if (is_null($user))
            return null;
        if (!($user instanceof User))
            throw new \Exception("La entidad no es un User");
        return [
            'id' => $user->getId(),
            'username' => $user->getUsername(),
            'email' => $user->getEmail(),
            'rol' => $user->getRol()
        ];
    }

    public function getAll()
    {
        $users = $this->em->getRepository(User::class)->findAll();

        return $this->entitiesToArray($users);
    }

    public function profile()
    {
        $user = $this->getUser();
        return $this->toArray($user);
    }

    public function getTokenByEmail($email)
    {
        $user = $this->em->getRepository(User:: class )
            ->findOneBy(array('email'=>$email));

        if ( is_null ($user))
            throw new AccessDeniedHttpException('Usuario no autorizado');

        return $this->jwtManager->create($user);
    }

    public function cambiaPassword($nuevoPassword)
    {
        $user = $this->getUser();
        $user->setPassword($this->encoder->encodePassword($user, $nuevoPassword));
        return $this->guardaValidando($user);
    }

    public function cambiaEmail($nuevoEmail)
    {
        $user = $this->getUser();
        $user->setEmail($nuevoEmail);
        return $this->guardaValidando($user);
    }

    public function update(User $user, array $data, $dir)
    {
        $user->setName($data['name']);
        $user->setRol($data['rol']);
        $user->setCiudad($data['ciudad']);
        $user->setEmail($data['email']);

        $this->uploader->setTargetDir($dir);
        $fileName = $this->uploader->saveB64Imagen($data['avatar']);
        $user->setAvatar($fileName);

        return $this->guardaValidando($user);
    }

    public function getUsersSearch($search)
    {
        return $this->em->getRepository(User::class)->findUsers($search);
    }
}