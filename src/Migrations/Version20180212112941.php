<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180212112941 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE event ADD categoria_id INT DEFAULT NULL, DROP categoria');
        $this->addSql('ALTER TABLE event ADD CONSTRAINT FK_3BAE0AA73397707A FOREIGN KEY (categoria_id) REFERENCES category (id)');
        $this->addSql('CREATE INDEX IDX_3BAE0AA73397707A ON event (categoria_id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE event DROP FOREIGN KEY FK_3BAE0AA73397707A');
        $this->addSql('DROP INDEX IDX_3BAE0AA73397707A ON event');
        $this->addSql('ALTER TABLE event ADD categoria VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, DROP categoria_id');
    }
}
